import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ListClient from '../views/Client/ListClient.vue';

/**
 * MEDICAMENT IMPORT
 */
import AddCategorie from '../views/Medicament/Categorie/AddCategorie.vue';
import ListCategorie from '../views/Medicament/Categorie/ListCategorie.vue';
import EditCategorie from '../views/Medicament/Categorie/EditCategorie.vue';

import AddTypeMedicament from '../views/Medicament/TypeMedicament/AddTypeMedicament.vue';
import ListTypeMedicament from '../views/Medicament/TypeMedicament/ListTypeMedicament.vue';
import EditTypeMedicament from '../views/Medicament/TypeMedicament/EditTypeMedicament.vue';

import AddUnit from '../views/Medicament/Unit/AddUnit.vue';
import ListUnit from '../views/Medicament/Unit/ListUnit.vue';
import EditUnit from '../views/Medicament/Unit/EditUnit.vue';

import AddMedicament from '../views/Medicament/AddMedicament.vue';
import ListMedicament from '../views/Medicament/ListMedicament.vue';
import EditMedicament from '../views/Medicament/EditMedicament.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, 

  {
    path: '/client',
    name: 'Client',
    component: ListClient
  },
  /*************MEDICAMENT **********************/
  {
    path:'/medicament-add',
    name:'medicament-add',
    component:AddMedicament
  },
  {
    path:'/medicament',
    name:'medicament-list',
    component:ListMedicament
  },
  {
    path:'/medicament-edit-:id',
    name:'medicament-edit',
    props:true,
    component:EditMedicament
  },
  //Categorie
  {
    path:'/categorie-add',
    name:'categorie-add',
    component:AddCategorie
  },
  {
    path:'/categorie',
    name:'categorie-list',
    component:ListCategorie
  },
  {
    path:'/categorie-edit-:id',
    name:'categorie-edit',
    props: true,
    component:EditCategorie
  },
//TypeMedicament
{
  path:'/type-medicament-add',
  name:'type-medicament-add',
  component:AddTypeMedicament
},
{
  path:'/type-medicament',
  name:'type-medicament-list',
  component:ListTypeMedicament
},
{
  path:'/type-medicament-edit-:id',
  name:'type-medicament-edit',
  props: true,
  component:EditTypeMedicament
},
  //unit
  {
    path:'/unit-add',
    name:'unit-add',
    component:AddUnit
  },
  {
    path:'/unit',
    name:'unit-list',
    component:ListUnit
  },
  {
    path:'/unit-edit-:id',
    name:'unit-edit',
    props:true,
    component:EditUnit
  },

  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
